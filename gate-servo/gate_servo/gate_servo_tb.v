// Timed gate control test bench

// Jorge Juan-Chico <jjchico@us.es>
// 2018-12-20

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module test();

    localparam SF = 1000;           // system frequency (Hz)
    localparam CYCLE = 10**9/SF;    // periode (ns)

    reg clk;      // open button (active high)
    reg button;   // obstacle sensor
    reg obs;      // system clock
    wire open;    // set to 1 in open state
    wire gate;    // gate control (set to 1 in open and clising states)
    wire servo;   // servo control signal

    // Design under test
    gate_servo #(.SYS_FREQ(SF)) dut (
        .clk(clk), .button(button), .obs(obs),
        .open(open), .gate(gate), .servo(servo)
        );

    // clock generator
    always
        #(CYCLE/2) clk = ~clk;

    initial begin
        clk = 0;
        button = 0;
        obs = 0;

        // NOTE: use "vvp a.out -fst" to generate fst format
        $dumpfile("gate_servo_tb.fst");
        // we do not want to save servo module's signals, hence "1".
        $dumpvars(1, test);

        repeat(1*SF) @(negedge clk);    // wait 1s
        button = 1;                     // press for 1s
        repeat(1*SF) @(negedge clk);
        button = 0;
        repeat(3*SF) @(negedge clk);    // wait 3s
        obs = 1;                        // put an obstacle
        repeat(4*SF) @(negedge clk);    // wait 4s
        obs = 0;                        // remove obstacle
        repeat(4*SF) @(negedge clk);    // wait 4s
        obs = 1;                        // put an obstacle
        repeat(2*SF) @(negedge clk);    // wait 2s
        button = 1;                     // press 1 cycle
        @(negedge clk) button = 0;
        repeat(2*SF) @(negedge clk);    // wait 2s
        obs = 0;                        // remove obstacle
        repeat(8*SF) @(negedge clk);    // wait 8s
        $finish;
    end
endmodule
