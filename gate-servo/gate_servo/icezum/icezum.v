// Timed gate control. Icezum Alhambra board implementation

// Board and tools: https://github.com/FPGAwars/icezum

// Jorge Juan-Chico <jjchico@us.es>
// 2018-12-20

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

 // System frequency
 `define SYS_FREQ 12000000

`include "../gate_servo.v"

module icezum (
    input wire CLK,     // system clock
    input wire SW1,     // open button
    input wire D13,     // obstacle sensor
    output wire LED0,   // open indicator
    output wire LED1,   // gate control indicator
    output wire D0      // servo control signal
    );

    gate_servo #(
            .SYS_FREQ(`SYS_FREQ)
        ) gate (
            .clk(CLK), .button(SW1), .obs(D13),
            .open(LED0), .gate(LED1), .servo(D0)
        );

endmodule
