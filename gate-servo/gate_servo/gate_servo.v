// Timed gate control
// Description: When the input is activated, the gate is open. It closes
//     automatically after some time. Digital and servo output using the
//     1-bit servo controller module.

// Jorge Juan-Chico <jjchico@us.es>
// Initial date: 2018-12-20

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

`include "../../servo-driver/servo.v"
`include "../../servo-driver/servo_1bit.v"

module gate_servo #(
    parameter SYS_FREQ = 12000000,  // system frquency
    parameter DELAY = 7             // time the gate remains open
    )(
    input wire clk,       // system clock
    input wire button,    // open button (active high)
    input wire obs,       // obstacle sensor (active low)
    output wire open,     // set to 1 in open state
    output wire gate,     // gate control (set to 1 in open and closing states)
    output wire servo     // servo control signal
    );

    localparam MAXCOUNT = SYS_FREQ * DELAY;

    // states
    localparam  [1:0] CLOSED = 2'b00,
                OPEN = 2'b01,
                CLOSING = 2'b11;

    reg [31:0] count = 0;       // time counter
    reg [1:0] state = CLOSED;   // state

    // Let's try a monolithic approach :)
    // (state machine and counter in the same process)
    always @(posedge clk)
        case(state)
        CLOSED:
            if(button)
                state <= OPEN;
        OPEN:
            if(button)
                count <= 0;
            else if (count == MAXCOUNT) begin
                count <= 0;
                if(!obs)
                    state <= CLOSING;
                else
                    state <= CLOSED;
            end else
                count = count + 1;
        CLOSING:
            if(button)
                state <= OPEN;
            else if(obs)
                state <= CLOSED;
        endcase

    // gate output state indicators
    assign open = state == OPEN ? 1'b1 : 1'b0;
    assign gate = state != CLOSED ? 1'b1 : 1'b0;

    // servo control
    servo_1bit #(.FS(SYS_FREQ)) servo_1bit (.clk(clk), .a(gate), .s(servo));

endmodule
