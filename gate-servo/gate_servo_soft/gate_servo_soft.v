// Timed gate control. Slow closing/opening gate.
// Description: When the input is activated, the gate is open. It closes
//     automatically after some time. Gate opens if an obstacle is detected
//     while closing. Digital and servo output using the 1-bit servo controller
//     module (soft operation variant).

// Jorge Juan-Chico <jjchico@us.es>
// Initial date: 2019-01-22

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

`include "../../servo-driver/servo.v"
`include "../../servo-driver/servo_soft.v"
`include "../../servo-driver/servo_soft_1bit.v"

module gate_servo_soft #(
    parameter SYS_FREQ = 12000000,  // system frquency
    parameter DELAY = 10,           // time the gate remains open
    parameter SERVO_DELAY = 5       // open/closing time
    )(
    input wire clk,       // system clock
    input wire button,    // open button (active high)
    input wire obs,       // obstacle sensor (active low)
    output wire finished, // set to 1 when gate movement finishes
    output wire gate,     // gate control (0-close, 1-open)
    output wire servo     // servo control signal
    );

    localparam MAXCOUNT = SYS_FREQ * DELAY;

    // states
    localparam  [1:0] CLOSED  = 2'd0,
                      OPEN    = 2'd1,
                      CLOSING = 2'd2,
                      WAITING = 2'd3;

    reg [31:0] count = 0;       // time counter
    reg [1:0] state = CLOSED;   // state

    // Let's try a monolithic approach :)
    // (state machine and counter in the same process)
    always @(posedge clk)
        case(state)
        CLOSED:
            if(button)
                state <= OPEN;
        OPEN:
            if(button)
                count <= 0;
            else if (count == MAXCOUNT) begin
                count <= 0;
                state <= WAITING;
            end else
                count = count + 1;
        WAITING:
            if(obs)        // no obstacle
                state <= CLOSING;
        CLOSING:
            if(button)
                state <= OPEN;
            else if(!obs)       // obstacle
                state <= WAITING;
            else if(finished)
                state <= CLOSED;
        endcase

    // gate output state indicators
    assign gate = (state == OPEN || state == WAITING) ? 1'b1 : 1'b0;

    // servo control
    servo_soft_1bit #(.FS(SYS_FREQ), .DELAY(SERVO_DELAY))
        servo_1bit (.clk(clk), .a(gate), .s(servo), .f(finished));

endmodule
