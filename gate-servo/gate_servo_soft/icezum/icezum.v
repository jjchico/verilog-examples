// Timed gate control. Icezum Alhambra board implementation

// Board and tools: https://github.com/FPGAwars/icezum

// Jorge Juan-Chico <jjchico@us.es>
// 2018-12-20

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

// System frequency
`define SYS_FREQ 12000000

`include "../gate_servo_soft.v"

module icezum (
    input wire CLK,     // system clock
    input wire D13,     // open button
    input wire SW1,     // alternative open button
    input wire D8,      // obstacle sensor
    output wire LED0,   // open indicator
    output wire LED1,   // gate control indicator
    output wire D0      // servo control signal
    );

    gate_servo_soft #(
            .SYS_FREQ(`SYS_FREQ)
        ) gate (
            .clk(CLK), .button(D13|SW1), .obs(D8),
            .finished(LED0), .gate(LED1), .servo(D0)
        );

endmodule
