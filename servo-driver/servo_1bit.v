// Module: servo_1bit
// Depends on: servo
// Description: 1 bit servo controller
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 2018-12-21

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
   One bit servo controller using the "servo" module. Set the servo angle to
   the minimum or maximum value depending on one bit input.

   Input/output signals

     * clk: system clock (this is a synchronous design)
     * a: target angle.
           a=0 set angle to minimum value.
           a=1 set angle to maximum value.
     * s: output control signal. Connect to servo control input.

   Parameters

   It uses the same set of parameters than the servo driver. Parameter values
   are passed to the servo driver. See servo driver code and documentation for
   details.

   System clock frequency (FS) must be set in all cases. The rest have typical
   defaults for most servos.

     * FS: system clock frequency in Hz (defaults to 16MHz). ALWAYS SET TO
       SYSTEM'S FREQUENCY!
     * N: angle resolution in bits (defaults to 8: -128 -- +127 range, 0
       neutral). Values above 10 are probably not useful.
     * TFU: time frame in microseconds (defaults to 20000us = 20ms).
     * TCU: neutral time in microseconds (defaults to 1500us = 1.5ms).
     * RU: range in microseconds (defaults to 1000us = 1ms).
 */

`timescale 1ns / 1ps

module servo_1bit #(
    parameter FS = 16000000,        // clock frequency (Hz)
    parameter N = 8,                // Control input width in bits
    parameter TFU = 20000,          // servo refresh cycle -timeframe- (us)
    parameter TCU = 1500,           // neutral time (us)
    parameter RU = 1000             // time range (us)
    )(
    input wire clk,                 // system clock
    input wire a,                   // servo angle (0-minimum, 1-maximum)
    output wire s                   // output signal
    );

    wire signed [N-1:0] angle;

    servo #(.FS(FS), .N(N), .TFU(TFU), .TCU(TCU), .RU(RU))
        servo (.clk(clk), .a(angle), .s(s));

    assign angle = a == 1'b0 ? -2**(N-1) : 2**(N-1)-1;

endmodule
