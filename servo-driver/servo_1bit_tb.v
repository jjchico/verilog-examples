// Design: servo 1bit
// Module: test
// Depends on: servo_1bit
// Description: 1 bit servo controller test bench
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 2018-12-21

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
 * Test bench for the servo 1bit controller
 */

`define FS 1000000.0 // Hz
`define NBITS 8

`timescale 1ns / 1ps

module test;

    localparam real HCYCLE = 500000000.0/`FS;
    localparam N = `NBITS;

    reg clk;
    reg a;
    wire s;

    servo_1bit #(
        .FS(`FS),
        .N(N),
        .TFU(20000),
        .TCU(1500),
        .RU(1000)
        ) uut (.clk(clk), .a(a), .s(s));

    initial begin
        // Waveform generation
        $dumpfile("servo_1bit_tb.vcd");
        $dumpvars(1, test);

        clk = 0;

        a = 1'b0;                   // neutral position
        repeat (3) @(posedge s);

        a = 1'b1;                   // maximum angle
        repeat (2) @(posedge s);

        a = 1'b0;                   // minimum angle
        repeat (2) @(posedge s);

        $finish;

    end

    // clock generation
    always
        #(HCYCLE) clk = ~clk;
endmodule
