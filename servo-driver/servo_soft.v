// Module: servo_soft
// Depends on: servo
// Description: Soft (speed-configurable) servo controller
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 2019-01-18

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
   Wrapper to the servo controller that makes the servo to move slowly to the
   target angle. Speed is configurable through DELAY parameter. It works as
   a simple constant-speed trayectory generator: the current angle given to
   the servo controller is slowly incresed until it reaches the target angle.
   An output signal 'f' indicates when the target angle is reached.

   Input/output signals

     * clk: system clock (this is a synchronous design)
     * a: target angle. This is a signed value with a configurable number of
         bits (N).
           a=0 is the neutral position (typ. 1.5ms).
           a=2^(N-1)-1 is the top of the range (typ. 2ms)
           a=-2^(N-1) is the bottom of the range (typ. 1ms).
     * s: output control signal. Connect to servo control input.
     * f: output state signal. Set to 1 when target angle is reached.

   Parameters

   System clock frequency (FS) must be set in all cases. The rest have typical
   defaults for most servos.

     * FS: system clock frequency in Hz (defaults to 16MHz). ALWAYS SET TO
       SYSTEM'S FREQUENCY!
     * N: angle resolution in bits (defaults to 8: -128 -- +127 range, 0
       neutral). Values above 10 are probably not useful.
     * TFU: time frame in microseconds (defaults to 20000us = 20ms).
     * TCU: neutral time in microseconds (defaults to 1500us = 1.5ms).
     * RU: range in microseconds (defaults to 1000us = 1ms).
     * DELAY: time spent to cover the whole range of the servo. In seconds
       (integer). 0 means the trajectory is generated as fast as possible.
       Defaults to 4.
 */

`timescale 1ns / 1ps

module servo_soft #(
    parameter FS = 16000000,        // clock frequency (Hz)
    parameter N = 8,                // Control input width in bits
    parameter TFU = 20000,          // servo refresh cycle -timeframe- (us)
    parameter TCU = 1500,           // neutral time (us)
    parameter RU = 1000,            // time range (us)
    parameter DELAY = 4             // whole range delay (s)
    )(
    input wire clk,                 // system clock
    input wire signed [N-1:0] a,    // servo target angle
    output wire s,                  // output signal
    output wire f                   // target angle reached
    );

    wire enable;                    // trayectory generator enable
    reg signed [N-1:0] oa = 'd0;    // output angle

    // Servo driver
    servo #(.FS(FS), .N(N), .TFU(TFU), .TCU(TCU), .RU(RU)) servo
        (.clk(clk), .a(oa), .s(s));

    // If DELAY is 0 the tayectory generator is always enabled to operate as
    // fast as possible. If not, a prescaler is configured to execute the
    // trayectory smoothly
    if (DELAY == 0)
        assign enable = 1'b1;
    else begin
        // Prescaler modulo: the output angle will be incresed/decreased
        // by 1 every PMOD clk ticks
        localparam integer PMOD = (FS*DELAY)/2**N;
        localparam PW = $clog2(PMOD);       // prescaler bit-width

        // Prescaler counter
        reg [PW-1:0] pcount = 0;
        // Prescaler
        always @(posedge clk)
            if (pcount < PMOD-1)
                pcount <= pcount+1;
            else
                pcount <= 0;
        assign enable = (pcount == PMOD-1) ? 1'b1 : 1'b0;
    end

    // Update current angle (trayectory generator)
    always @(posedge clk)
        if (enable)
            // increment main counter
            if (a < oa)
                oa <= oa - 1;
            else if (a > oa)
                oa <= oa + 1;

    // Finished output
    assign f = (a == oa) ? 1'b1 : 1'b0;

endmodule
