// Design: servo
// Description: Servo demo design
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 20 nov. 2018

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
   Servo controller demo design.

   Moves the servo to different positions.
 */

`timescale 1ns / 1ps

module servo_demo #(
    parameter FS = 16000000     // system clock frequency (Hz)
                                // define the actual value in your top level
    )(
    input clk,          // system clock
    output wire s,      // servo control
    output wire start   // start position indicator
    );

    // Change to desired values
    localparam N = 8;           // servo resolutions (bits)
    localparam DELAY = 2000;    // delay between positions (ms)

    localparam integer DIVMOD = FS/1000*DELAY;
    localparam DW = $clog2(DIVMOD);
    localparam  MAX = 2**(N-1)-1;
    localparam signed MIN = -2**(N-1);

    reg [DW-1:0] fd;            // frequency divider
    reg [2:0] pos;              // position counter
    reg signed [N-1:0] angle;   // servo angle

    // servo controller
    servo #(            // Change TFU, TCU or RU to desired values
        .FS(FS),        // clock frequency (Hz)
        .N(N),          // resolution (bits). Ex: 8
        .TFU(20000),    // frame time (us). Ex: 20000
        .TCU(1500),     // centre time (us). Ex: 1500
        .RU(1000)       // range (us). Ex: 1000
        ) servo_test (.clk(clk), .a(angle), .s(s));

    // frequency divider
    always @(posedge clk)
        if (fd < DIVMOD-1)
            fd <= fd + 1;
        else
            fd <= 0;

    // position
    always @(posedge clk)
        if (fd == 0)
            pos <= pos + 1;

    // output generator
    always @*
        case (pos)
        0: angle = 0;       // neutral
        1: angle = 0;
        2: angle = MIN;     // low limit
        3: angle = MIN>>1;  // low half
        4: angle = 0;       // neutral
        5: angle = MAX>>1;  // high half
        6: angle = MAX;     // high limit
        7: angle = 0;       // neutral
        endcase

    // start position indicator
    assign start = pos == 0 ? 1'b1 : 1'b0;
endmodule
