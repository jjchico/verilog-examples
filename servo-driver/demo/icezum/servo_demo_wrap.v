// Design: servo
// Description: Servo demo Icezum Alhambra wrapper
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 20 nov. 2018

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
 * Servo controller demo wrapper for the Icezum Alhambra board.
 * Icezum Alhambra information: https://github.com/FPGAwars/icezum
 *
 * Inputs: none
 * Outputs:
 *      * D0: servo control signal (s)
 *      * LED0: start position idicator
 */

// Board's clock frequency (Hz)
`define BOARD_CLK_FREQ 12000000

// Include demo circuit and servo driver
`include "../servo_demo.v"
`include "../../servo.v"

module top (
    input wire CLK,     // system clock
    output wire D0,     // servo control
    output wire LED0    // start
    );

    servo_demo #(
        .FS(`BOARD_CLK_FREQ),
        ) servo_demo (.clk(CLK), .s(D0), .start(LED0));

endmodule
