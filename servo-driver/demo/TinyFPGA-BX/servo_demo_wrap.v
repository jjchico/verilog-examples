// Design: servo
// Description: Servo demo TinyFPGA-BX wrapper
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 20 nov. 2018

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
 * Servo controller demo wrapper for the TinyFPGA-BX board.
 * TinyFPGA BX information: https://github.com/tinyfpga/TinyFPGA-BX/
 *
 * Inputs: none
 * Outputs:
 *      * PIN_13: servo control signal (s)
 *      * LED: start position idicator
 */

// Board's clock frequency (Hz)
`define BOARD_CLK_FREQ 16000000

// Include demo circuit and servo driver
`include "../servo_demo.v"
`include "../../servo.v"

module top (
    input wire CLK,         // system clock
    output wire PIN_1,      // servo control
    output wire LED         // start
    );

    servo_demo #(
        .FS(`BOARD_CLK_FREQ),
        ) servo_demo (.clk(CLK), .s(PIN_1), .start(LED));

endmodule
