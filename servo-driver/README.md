# Verilog servo driver for hobby (RC) servos

This servo controller will generate the control signal for a typical hobby
servo like the ones used in radio control (RC) applications given a target
angle. The module is fully parameterized so it can be adapted to any
servo, including continuous rotating servos.

Also included is a *soft* (variable speed) version and a 1-bit variant that
moves the servo to the beginning or end of the range.

For a quick start see the [Examples](#examples) section below.

![Demo video](https://www.youtube.com/watch?v=OFlehUeUzIE)

## Servo motors operation

Servo motors can rotate to a fixed angle in a range. Typical ranges are 90
degrees and 180 degrees. Servos are controlled by a sending digital pulses to
the control signal of the servo. The width of the pulse is proportional to the
target angle of the servo and pulses are repeated at some frame rate, defining
the timeframe. Typical values are:

* Timeframe: 20ms
* Servo's neutral position pulse width: 1.5ms
* Pulse width range: 1ms to 2ms

Thus, a typical servo can be controlled with pulses of width from 1 to 2ms.
The angle range depends on the servo. Typical ranges are 90 deg. and 180 deg.
Many servos can be driven outside their nominal range, but it may damage
the servo if it exceeds the mechanical limits of the device.

Continuous-rotation servos can be obtained by modifying regular servos, but
there are also some models available commercially. This servos open the
feedback loop and eliminate the physical restrictions of the device, so any
target angle different from the neutral position will make the servo to
rotate either clockwise or counter-clockwise. The rotation speed can be
controlled with the target angle.

To know more see
[https://en.wikipedia.org/wiki/Servo_(radio_control)][servo].

[servo]: https://en.wikipedia.org/wiki/Servo_(radio_control)

## Servo drivers

* **Basic driver**: module *servo* in file *servo.v*. Moves inmediatly to the
  target angle.

* **Soft driver**: module *servo_soft* in file *servo_soft.v*. Moves the servo
  to the targe angle at constant speed.

* **1-bit variants**: moves the servo to the beginning or end of the range.

You must always set the *FS* parameter with the frequency of the clock driving
the servo in Hz.

### Input/output signals

* **clk**: system clock (this is a synchronous design)
* **a (basic and soft drivers)**: target angle. This is a signed value with a
  configurable number of bits (N).
  - a=0 is the neutral position.
  - a=2^(N-1)-1 is the top of the range.
  - a=-2^(N-1) is the bottom of the range.
* **a (1-bit variants)**: target angle.
    - a=0 set angle to the minimum value.
    - a=1 set angle to the maximum value.
* **s**: output control signal. Connect to servo's control input.
* **f**: (soft version only) target angle reached (finished).

### Parameters

* **FS**: system clock frequency (in Hz). This is a mandatory parameter. Since
  the control signal depends on accurate absolute timing, the controller will
  only work correctly if it is configured with the actual frequency of the
  system clock (clk) driving the controller. Defaults to 16MHz, but it will not
  work if the system clock is not exactly that.

* **N**: angle resolution in bits (defaults to 8). With the default value,
  the control angle ranges from -128 to +127, with 0 being the neutral
  position. Can be set to a higher value for a finer control, but values
  above 10 are probably not useful.

* **TFU**: time frame in microseconds (defaults to 20ms). The default is
  the standard for most servos, but they will typically tolerate
  variations, so you can experiment. E.g. a value of 10000 (10ms) will
  make the servo to update the position at double the standard rate.

* **TCU**: neutral time in microseconds (defaults to 1.5ms). The default is
  the standard value. Can be used to adjust the neutral position or to
  calibrate continuous rotating servos. Attention: big changes may make
  the servo to hit the mechanical limits of the device.

* **RU**: range in microseconds (defaults to 1ms). The default is the standard
  value. It can be used to extend the nominal range of the servo
  (attention, it may damage the device), to limit the nominal range
  (e.g. to make a 180 deg. servo to behave like a 90 deg. servo) or to
  limit the maximum/minimum speed in a continuous rotating servo.

* **DELAY**: (soft version only) time spent to cover the whole range of the
  servo, in seconds (integer). 0 means the trajectory is generated as fast as
  possible. Defaults to 4.

## Examples

*FCLK* is the clock frequency driving the servo. It is the only mandatory
parameter.

Driving a servo with standard parameters:

    // SG90 90 deg. range (standard parameters)
    servo #(.FS(FCLK)) servo1 (.clk(clk), .a(servo1_in), .s(servo1_out));

Driving a servo with custom parameters to obtain a 180 deg. range:

    // SM-S2309S 180 deg. range
    servo #(.FS(FCLK), .TCU(1250), .RU(1550)) servo2 (
        .clk(clk), .a(servo2_in), .s(servo2_out));

Use a longer refresh rate (30ms) and a slightly larger range (1.2ms)

    servo #(.FS(FCLK), .TFU(30000), .RU(1200)) myservo (
        .clk(clk), .a(angle), .s(servo_ctl));

Moves the servo slowly to the target. Full range delay is 5s.

    servo_soft #(.FS(FCLK), .DELAY(5)) myservo (
        .clk(clk), .a(angle), .s(servo_ctl), .f(finished));

Use a servo to control an automatic gate. Open/close time is 7s.

    servo_soft_1bit #(.FS(FCLK), .DELAY(7)) gate_servo (
        .clk(clk), .a(open), .s(servo_ctl), .f(end_reached));


### Sample configurations

Note that servo characteristics can vary from unit to unit, even for the same
device number. These are results from some tested servos. Feel free to send
your results.

| Servo     | TCU (us) | RU (us) | Range (deg.) | Param. type |
|-----------|----------|---------|--------------|-------------|
| SG90      |     1500 |    1000 |           90 | standard    |
| SM-S2309S |     1250 |    1550 |          180 | custom      |

## Testing

### Test bench

You can use the test benches in files ending in *_tb.v* to
simulate the modules.

For example, using Icarus Verilog & Gtkwave to simulate module *servo*:

    $ iverilog servo_tb.v servo.v
    $ vvp a.out
    $ gtkwave servo_tb.vcd servo_tb.gtkw &

### Demo circuit

Demo circuits for the *servo* and *servo_soft* modules are included in the
*demo* and *demo_soft* folders respectively. The demos moves the servo to a
sequence of positions. Check the demos' source code to change the sequence of
the demo and/or the servo parameters.

FPGA top-level designs ready for implementation are included with the demos
for some boards:

* TinyFPGA-BX: supported by the [apio][apio] toolbox.
* Icezum Alhambra: supported by the [apio][apio] toolbox.

[Apio][apio] is a front-end to a completely free (as in freedom) toolchain for
synthesis on some FPGA chips. To implement the servo demo in one of the
apio-supported boards, just enter the folder of the board and run:

    $ apio build
    $ apio upload

Read the top-level wrapper design of the board to see the used pins in each
board. It should be easy to port the demo circuit to any FPGA design platform by
adding a wrapper top-level and a restrictions file.

You can use the demo circuit to test your servos and adjust the centre time
(TCU) and range (RU) of the driver.

[apio]: https://github.com/FPGAwars/apio

## Authors

  * **Jorge Juan-Chico**

## Licence

This is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version. See
<http://www.gnu.org/licenses/>.
