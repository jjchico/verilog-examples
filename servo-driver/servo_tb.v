// Design: servo
// Description: Servo driver test bench
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 20 nov. 2018

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
 * Test bench for the servo driver
 */

`define FS 1000000.0 // Hz
`define NBITS 8

`timescale 1ns / 1ps

module test;

    localparam real HCYCLE = 1000000000.0/`FS/2.0;  // half clock period in ns
    localparam N = `NBITS;

    reg clk;
    reg signed [`NBITS-1:0] a;
    wire s;

    servo #(
        .FS(`FS),
        .N(N),
        .TFU(20000),
        .TCU(1500),
        .RU(1000)
        ) uut (.clk(clk), .a(a), .s(s));

    initial begin
        // Waveform generation
        // with Icarus Verilog, use "vvp a.out -f" to generate fst format
        $dumpfile("servo_tb.fst");
        $dumpvars(0, test);

        clk = 0;

        a = 0;                      // neutral position
        repeat (3) @(posedge s);

        a = -2**(N-1);              // minimum angle
        repeat (2) @(posedge s);

        a = 2**(N-1)-1;             // maximum angle
        repeat (2) @(posedge s);

        $finish;

    end

    // clock generation
    always
        #(HCYCLE) clk = ~clk;
endmodule
