// Design: servo
// Description: Servo driver for hobby servo motors
// Author: Jorge Juan-Chico <jjchico@gmail.com>
// Initial date: 20 nov. 2018

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. See <http://www.gnu.org/licenses/>.                     //
////////////////////////////////////////////////////////////////////////////////

/*
   This servo controller will generate the control signal for a typical hobby
   servo like the ones used in RC applications taking a target angle as
   input. The module is fully parameterized so it can be addapted to any
   servo, including continuous rotating servos.

   Input/output signals

     * clk: system clock (this is a synchronous design)
     * a: target angle. This is a signed value with a configurable number of
         bits (N).
           a=0 is the neutral position (typ. 1.5ms).
           a=2^(N-1)-1 is the top of the range (typ. 2ms)
           a=-2^(N-1) is the bottom of the range (typ. 1ms).
     * s: output control signal. Connect to servo control input.

   Parameters

   System clock frequency (FS) must be set in all cases. The rest have typical
   defaults for most servos.

     * FS: system clock frequency in Hz (defaults to 16MHz). ALWAYS SET TO
       SYSTEM'S FREQUENCY!
     * N: angle resolution in bits (defaults to 8: -128 -- +127 range, 0
       neutral). Values above 10 are probably not useful.
     * TFU: time frame in microseconds (defaults to 20000us = 20ms).
     * TCU: neutral time in microseconds (defaults to 1500us = 1.5ms).
     * RU: range in microseconds (defaults to 1000us = 1ms).
 */

`timescale 1ns / 1ps

module servo #(
    parameter FS = 16000000,        // clock frequency (Hz)
    parameter N = 8,                // Control input width in bits
    parameter TFU = 20000,          // servo refresh cycle -timeframe- (us)
    parameter TCU = 1500,           // neutral time (us)
    parameter RU = 1000             // time range (us)
    )(
    input wire clk,                 // system clock
    input wire signed [N-1:0] a,    // servo angle
    output reg s                    // output signal
    );

    localparam real MICRO = 0.000001;   // micro factor

    // Convert time to seconds
    localparam real TF = TFU*MICRO;
    localparam real TC = TCU*MICRO;
    localparam real R = RU*MICRO;

    // Counters parameter calculations
    localparam real FP = 2**N/R;        // ideal prescaler frequency (Hz)
    localparam integer PMOD = FS/FP;    // prescaler modulo (rounded to int)
    localparam real FPA = FS/PMOD;      // actual prescaler freq.
    localparam integer CMOD = TF*FPA;   // main counter modulo (rounded to int)
    localparam integer CC = TC*FPA;     // centre count

    localparam PW = $clog2(PMOD);       // prescaler bit-width
    localparam CW = $clog2(CMOD);       // main counter bit-width

    // Internal signals
    reg [PW-1:0] pcount = 0;        // prescaler counter
    reg [CW-1:0] count = 0;         // main counter
    reg signed [N-1:0] areg;        // registered angle
    wire [CW-1:0] coff;             // counter's off cycle

    // Prescaler
    always @(posedge clk)
        if (pcount < PMOD-1)
            pcount <= pcount+1;
        else
            pcount <= 0;

    // counter's off cycle
    assign coff = CC + areg;

    // Input/output and main caounter
    always @(posedge clk)
        if (pcount == PMOD-1) begin
            // increment main counter
            if (count < CMOD-1)
                count <= count + 1;
            else
                count <= 0;

            // register input
            areg <= a;

            // generate output
            if (count < coff)
                s <= 1'b1;
            else
                s <= 1'b0;
        end
endmodule
